import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Promocode } from './promocode.model';
import { PromocodeService } from './promocode.service';

@Component({
    selector: 'page-promocode',
    templateUrl: 'promocode.html'
})
export class PromocodePage {
    promocodes: Promocode[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private promocodeService: PromocodeService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.promocodes = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.promocodeService.query().pipe(
            filter((res: HttpResponse<Promocode[]>) => res.ok),
            map((res: HttpResponse<Promocode[]>) => res.body)
        )
        .subscribe(
            (response: Promocode[]) => {
                this.promocodes = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Promocode) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/promocode/new');
    }

    edit(item: IonItemSliding, promocode: Promocode) {
        this.navController.navigateForward('/tabs/entities/promocode/' + promocode.id + '/edit');
        item.close();
    }

    async delete(promocode) {
        this.promocodeService.delete(promocode.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Promocode deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(promocode: Promocode) {
        this.navController.navigateForward('/tabs/entities/promocode/' + promocode.id + '/view');
    }
}
