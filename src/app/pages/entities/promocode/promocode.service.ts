import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Promocode } from './promocode.model';

@Injectable({ providedIn: 'root'})
export class PromocodeService {
    private resourceUrl = ApiService.API_URL + '/promocodes';

    constructor(protected http: HttpClient) { }

    create(promocode: Promocode): Observable<HttpResponse<Promocode>> {
        return this.http.post<Promocode>(this.resourceUrl, promocode, { observe: 'response'});
    }

    update(promocode: Promocode): Observable<HttpResponse<Promocode>> {
        return this.http.put(this.resourceUrl, promocode, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<Promocode>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<Promocode[]>> {
        const options = createRequestOption(req);
        return this.http.get<Promocode[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
