import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Location } from './location.model';
import { LocationService } from './location.service';

@Component({
    selector: 'page-location-update',
    templateUrl: 'location-update.html'
})
export class LocationUpdatePage implements OnInit {

    location: Location;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        fullAddress: [null, []],
        floor: [null, []],
        room: [null, []],
        lat: [null, []],
        lon: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private locationService: LocationService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.location = response.data;
            this.isNew = this.location.id === null || this.location.id === undefined;
        });
    }

    updateForm(location: Location) {
        this.form.patchValue({
            id: location.id,
            fullAddress: location.fullAddress,
            floor: location.floor,
            room: location.room,
            lat: location.lat,
            lon: location.lon,
        });
    }

    save() {
        this.isSaving = true;
        const location = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.locationService.update(location));
        } else {
            this.subscribeToSaveResponse(this.locationService.create(location));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Location>>) {
        result.subscribe((res: HttpResponse<Location>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `Location ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/location');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): Location {
        return {
            ...new Location(),
            id: this.form.get(['id']).value,
            fullAddress: this.form.get(['fullAddress']).value,
            floor: this.form.get(['floor']).value,
            room: this.form.get(['room']).value,
            lat: this.form.get(['lat']).value,
            lon: this.form.get(['lon']).value,
        };
    }

}
