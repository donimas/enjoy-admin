import { BaseEntity } from 'src/model/base-entity';

export class Location implements BaseEntity {
    constructor(
        public id?: number,
        public fullAddress?: string,
        public floor?: string,
        public room?: string,
        public lat?: string,
        public lon?: string,
    ) {
    }
}
