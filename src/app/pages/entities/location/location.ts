import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Location } from './location.model';
import { LocationService } from './location.service';

@Component({
    selector: 'page-location',
    templateUrl: 'location.html'
})
export class LocationPage {
    locations: Location[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private locationService: LocationService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.locations = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.locationService.query().pipe(
            filter((res: HttpResponse<Location[]>) => res.ok),
            map((res: HttpResponse<Location[]>) => res.body)
        )
        .subscribe(
            (response: Location[]) => {
                this.locations = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Location) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/location/new');
    }

    edit(item: IonItemSliding, location: Location) {
        this.navController.navigateForward('/tabs/entities/location/' + location.id + '/edit');
        item.close();
    }

    async delete(location) {
        this.locationService.delete(location.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Location deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(location: Location) {
        this.navController.navigateForward('/tabs/entities/location/' + location.id + '/view');
    }
}
