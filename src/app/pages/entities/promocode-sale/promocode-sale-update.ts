import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { PromocodeSale } from './promocode-sale.model';
import { PromocodeSaleService } from './promocode-sale.service';
import { Promocode, PromocodeService } from '../promocode';

@Component({
    selector: 'page-promocode-sale-update',
    templateUrl: 'promocode-sale-update.html'
})
export class PromocodeSaleUpdatePage implements OnInit {

    promocodeSale: PromocodeSale;
    promocodes: Promocode[];
    startDate: string;
    endDate: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        sale: [null, []],
        minPrice: [null, []],
        maxPrice: [null, []],
        minAmount: [null, []],
        maxAmount: [null, []],
        minShare: [null, []],
        startDate: [null, []],
        endDate: [null, []],
        promocode: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private promocodeService: PromocodeService,
        private promocodeSaleService: PromocodeSaleService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.promocodeService.query()
            .subscribe(data => { this.promocodes = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.promocodeSale = response.data;
            this.isNew = this.promocodeSale.id === null || this.promocodeSale.id === undefined;
        });
    }

    updateForm(promocodeSale: PromocodeSale) {
        this.form.patchValue({
            id: promocodeSale.id,
            sale: promocodeSale.sale,
            minPrice: promocodeSale.minPrice,
            maxPrice: promocodeSale.maxPrice,
            minAmount: promocodeSale.minAmount,
            maxAmount: promocodeSale.maxAmount,
            minShare: promocodeSale.minShare,
            startDate: (this.isNew) ? new Date().toISOString() : promocodeSale.startDate,
            endDate: (this.isNew) ? new Date().toISOString() : promocodeSale.endDate,
            promocode: promocodeSale.promocode,
        });
    }

    save() {
        this.isSaving = true;
        const promocodeSale = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.promocodeSaleService.update(promocodeSale));
        } else {
            this.subscribeToSaveResponse(this.promocodeSaleService.create(promocodeSale));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<PromocodeSale>>) {
        result.subscribe((res: HttpResponse<PromocodeSale>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `PromocodeSale ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/promocode-sale');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): PromocodeSale {
        return {
            ...new PromocodeSale(),
            id: this.form.get(['id']).value,
            sale: this.form.get(['sale']).value,
            minPrice: this.form.get(['minPrice']).value,
            maxPrice: this.form.get(['maxPrice']).value,
            minAmount: this.form.get(['minAmount']).value,
            maxAmount: this.form.get(['maxAmount']).value,
            minShare: this.form.get(['minShare']).value,
            startDate: new Date(this.form.get(['startDate']).value),
            endDate: new Date(this.form.get(['endDate']).value),
            promocode: this.form.get(['promocode']).value,
        };
    }

    comparePromocode(first: Promocode, second: Promocode): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackPromocodeById(index: number, item: Promocode) {
        return item.id;
    }
}
