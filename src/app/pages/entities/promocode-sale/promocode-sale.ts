import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { PromocodeSale } from './promocode-sale.model';
import { PromocodeSaleService } from './promocode-sale.service';

@Component({
    selector: 'page-promocode-sale',
    templateUrl: 'promocode-sale.html'
})
export class PromocodeSalePage {
    promocodeSales: PromocodeSale[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private promocodeSaleService: PromocodeSaleService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.promocodeSales = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.promocodeSaleService.query().pipe(
            filter((res: HttpResponse<PromocodeSale[]>) => res.ok),
            map((res: HttpResponse<PromocodeSale[]>) => res.body)
        )
        .subscribe(
            (response: PromocodeSale[]) => {
                this.promocodeSales = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: PromocodeSale) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/promocode-sale/new');
    }

    edit(item: IonItemSliding, promocodeSale: PromocodeSale) {
        this.navController.navigateForward('/tabs/entities/promocode-sale/' + promocodeSale.id + '/edit');
        item.close();
    }

    async delete(promocodeSale) {
        this.promocodeSaleService.delete(promocodeSale.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'PromocodeSale deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(promocodeSale: PromocodeSale) {
        this.navController.navigateForward('/tabs/entities/promocode-sale/' + promocodeSale.id + '/view');
    }
}
