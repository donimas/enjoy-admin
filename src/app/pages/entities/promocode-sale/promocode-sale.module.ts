import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { PromocodeSalePage } from './promocode-sale';
import { PromocodeSaleUpdatePage } from './promocode-sale-update';
import { PromocodeSale, PromocodeSaleService, PromocodeSaleDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class PromocodeSaleResolve implements Resolve<PromocodeSale> {
  constructor(private service: PromocodeSaleService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PromocodeSale> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PromocodeSale>) => response.ok),
        map((promocodeSale: HttpResponse<PromocodeSale>) => promocodeSale.body)
      );
    }
    return of(new PromocodeSale());
  }
}

const routes: Routes = [
    {
      path: '',
      component: PromocodeSalePage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: PromocodeSaleUpdatePage,
      resolve: {
        data: PromocodeSaleResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: PromocodeSaleDetailPage,
      resolve: {
        data: PromocodeSaleResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: PromocodeSaleUpdatePage,
      resolve: {
        data: PromocodeSaleResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        PromocodeSalePage,
        PromocodeSaleUpdatePage,
        PromocodeSaleDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class PromocodeSalePageModule {
}
