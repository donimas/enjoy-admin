import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { PromocodeSale } from './promocode-sale.model';

@Injectable({ providedIn: 'root'})
export class PromocodeSaleService {
    private resourceUrl = ApiService.API_URL + '/promocode-sales';

    constructor(protected http: HttpClient) { }

    create(promocodeSale: PromocodeSale): Observable<HttpResponse<PromocodeSale>> {
        return this.http.post<PromocodeSale>(this.resourceUrl, promocodeSale, { observe: 'response'});
    }

    update(promocodeSale: PromocodeSale): Observable<HttpResponse<PromocodeSale>> {
        return this.http.put(this.resourceUrl, promocodeSale, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<PromocodeSale>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<PromocodeSale[]>> {
        const options = createRequestOption(req);
        return this.http.get<PromocodeSale[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
