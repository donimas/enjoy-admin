import { BaseEntity } from 'src/model/base-entity';
import { Menu } from '../menu/menu.model';

export const enum PriceCurrency {
    'KZT',
    'RU',
    'US'
}

export class PriceHistory implements BaseEntity {
    constructor(
        public id?: number,
        public price?: number,
        public currency?: PriceCurrency,
        public menu?: Menu,
    ) {
    }
}
