import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { PriceHistoryPage } from './price-history';
import { PriceHistoryUpdatePage } from './price-history-update';
import { PriceHistory, PriceHistoryService, PriceHistoryDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class PriceHistoryResolve implements Resolve<PriceHistory> {
  constructor(private service: PriceHistoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PriceHistory> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PriceHistory>) => response.ok),
        map((priceHistory: HttpResponse<PriceHistory>) => priceHistory.body)
      );
    }
    return of(new PriceHistory());
  }
}

const routes: Routes = [
    {
      path: '',
      component: PriceHistoryPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: PriceHistoryUpdatePage,
      resolve: {
        data: PriceHistoryResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: PriceHistoryDetailPage,
      resolve: {
        data: PriceHistoryResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: PriceHistoryUpdatePage,
      resolve: {
        data: PriceHistoryResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        PriceHistoryPage,
        PriceHistoryUpdatePage,
        PriceHistoryDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class PriceHistoryPageModule {
}
