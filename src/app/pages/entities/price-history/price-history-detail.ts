import { Component, OnInit } from '@angular/core';
import { PriceHistory } from './price-history.model';
import { PriceHistoryService } from './price-history.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-price-history-detail',
    templateUrl: 'price-history-detail.html'
})
export class PriceHistoryDetailPage implements OnInit {
    priceHistory: PriceHistory = {};

    constructor(
        private navController: NavController,
        private priceHistoryService: PriceHistoryService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.priceHistory = response.data;
        });
    }

    open(item: PriceHistory) {
        this.navController.navigateForward('/tabs/entities/price-history/' + item.id + '/edit');
    }

    async deleteModal(item: PriceHistory) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.priceHistoryService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/price-history');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
