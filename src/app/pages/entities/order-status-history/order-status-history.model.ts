import { BaseEntity } from 'src/model/base-entity';
import { Order } from '../order/order.model';

export const enum OrderStatus {
    'NEW',
    'CONFIRMED',
    'IN_PROCESS',
    'FINISHED',
    'DELIVERING',
    'DELIVERED',
    'CANCELED',
    'REJECTED'
}

export class OrderStatusHistory implements BaseEntity {
    constructor(
        public id?: number,
        public orderStatus?: OrderStatus,
        public createDate?: any,
        public comment?: string,
        public order?: Order,
    ) {
    }
}
