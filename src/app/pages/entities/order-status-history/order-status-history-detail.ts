import { Component, OnInit } from '@angular/core';
import { OrderStatusHistory } from './order-status-history.model';
import { OrderStatusHistoryService } from './order-status-history.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-order-status-history-detail',
    templateUrl: 'order-status-history-detail.html'
})
export class OrderStatusHistoryDetailPage implements OnInit {
    orderStatusHistory: OrderStatusHistory = {};

    constructor(
        private navController: NavController,
        private orderStatusHistoryService: OrderStatusHistoryService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.orderStatusHistory = response.data;
        });
    }

    open(item: OrderStatusHistory) {
        this.navController.navigateForward('/tabs/entities/order-status-history/' + item.id + '/edit');
    }

    async deleteModal(item: OrderStatusHistory) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.orderStatusHistoryService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/order-status-history');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
