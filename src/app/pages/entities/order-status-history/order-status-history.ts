import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { OrderStatusHistory } from './order-status-history.model';
import { OrderStatusHistoryService } from './order-status-history.service';

@Component({
    selector: 'page-order-status-history',
    templateUrl: 'order-status-history.html'
})
export class OrderStatusHistoryPage {
    orderStatusHistories: OrderStatusHistory[];

    // todo: add pagination

    constructor(
        private navController: NavController,
        private orderStatusHistoryService: OrderStatusHistoryService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.orderStatusHistories = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.orderStatusHistoryService.query().pipe(
            filter((res: HttpResponse<OrderStatusHistory[]>) => res.ok),
            map((res: HttpResponse<OrderStatusHistory[]>) => res.body)
        )
        .subscribe(
            (response: OrderStatusHistory[]) => {
                this.orderStatusHistories = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: OrderStatusHistory) {
        return item.id;
    }

    new() {
        this.navController.navigateForward('/tabs/entities/order-status-history/new');
    }

    edit(item: IonItemSliding, orderStatusHistory: OrderStatusHistory) {
        this.navController.navigateForward('/tabs/entities/order-status-history/' + orderStatusHistory.id + '/edit');
        item.close();
    }

    async delete(orderStatusHistory) {
        this.orderStatusHistoryService.delete(orderStatusHistory.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'OrderStatusHistory deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(orderStatusHistory: OrderStatusHistory) {
        this.navController.navigateForward('/tabs/entities/order-status-history/' + orderStatusHistory.id + '/view');
    }
}
