import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { OrderStatusHistoryPage } from './order-status-history';
import { OrderStatusHistoryUpdatePage } from './order-status-history-update';
import { OrderStatusHistory, OrderStatusHistoryService, OrderStatusHistoryDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class OrderStatusHistoryResolve implements Resolve<OrderStatusHistory> {
  constructor(private service: OrderStatusHistoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OrderStatusHistory> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<OrderStatusHistory>) => response.ok),
        map((orderStatusHistory: HttpResponse<OrderStatusHistory>) => orderStatusHistory.body)
      );
    }
    return of(new OrderStatusHistory());
  }
}

const routes: Routes = [
    {
      path: '',
      component: OrderStatusHistoryPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: OrderStatusHistoryUpdatePage,
      resolve: {
        data: OrderStatusHistoryResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: OrderStatusHistoryDetailPage,
      resolve: {
        data: OrderStatusHistoryResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: OrderStatusHistoryUpdatePage,
      resolve: {
        data: OrderStatusHistoryResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        OrderStatusHistoryPage,
        OrderStatusHistoryUpdatePage,
        OrderStatusHistoryDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class OrderStatusHistoryPageModule {
}
