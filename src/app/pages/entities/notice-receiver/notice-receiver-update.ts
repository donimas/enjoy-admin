import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { NoticeReceiver } from './notice-receiver.model';
import { NoticeReceiverService } from './notice-receiver.service';
import { Notice, NoticeService } from '../notice';

@Component({
    selector: 'page-notice-receiver-update',
    templateUrl: 'notice-receiver-update.html'
})
export class NoticeReceiverUpdatePage implements OnInit {

    noticeReceiver: NoticeReceiver;
    notices: Notice[];
    readDate: string;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        toUserId: [null, []],
        userLang: [null, []],
        flagSmsSent: ['false', []],
        flagEmailSent: ['false', []],
        flagPushSent: ['false', []],
        flagRead: ['false', []],
        readDate: [null, []],
        receiverFullName: [null, []],
        receiverPhone: [null, []],
        notice: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private noticeService: NoticeService,
        private noticeReceiverService: NoticeReceiverService
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        this.noticeService.query()
            .subscribe(data => { this.notices = data.body; }, (error) => this.onError(error));
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.noticeReceiver = response.data;
            this.isNew = this.noticeReceiver.id === null || this.noticeReceiver.id === undefined;
        });
    }

    updateForm(noticeReceiver: NoticeReceiver) {
        this.form.patchValue({
            id: noticeReceiver.id,
            toUserId: noticeReceiver.toUserId,
            userLang: noticeReceiver.userLang,
            flagSmsSent: noticeReceiver.flagSmsSent,
            flagEmailSent: noticeReceiver.flagEmailSent,
            flagPushSent: noticeReceiver.flagPushSent,
            flagRead: noticeReceiver.flagRead,
            readDate: (this.isNew) ? new Date().toISOString() : noticeReceiver.readDate,
            receiverFullName: noticeReceiver.receiverFullName,
            receiverPhone: noticeReceiver.receiverPhone,
            notice: noticeReceiver.notice,
        });
    }

    save() {
        this.isSaving = true;
        const noticeReceiver = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.noticeReceiverService.update(noticeReceiver));
        } else {
            this.subscribeToSaveResponse(this.noticeReceiverService.create(noticeReceiver));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<NoticeReceiver>>) {
        result.subscribe((res: HttpResponse<NoticeReceiver>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
          action = 'created';
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({message: `NoticeReceiver ${action} successfully.`, duration: 2000, position: 'middle'});
        toast.present();
        this.navController.navigateBack('/tabs/entities/notice-receiver');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

    private createFromForm(): NoticeReceiver {
        return {
            ...new NoticeReceiver(),
            id: this.form.get(['id']).value,
            toUserId: this.form.get(['toUserId']).value,
            userLang: this.form.get(['userLang']).value,
            flagSmsSent: this.form.get(['flagSmsSent']).value,
            flagEmailSent: this.form.get(['flagEmailSent']).value,
            flagPushSent: this.form.get(['flagPushSent']).value,
            flagRead: this.form.get(['flagRead']).value,
            readDate: new Date(this.form.get(['readDate']).value),
            receiverFullName: this.form.get(['receiverFullName']).value,
            receiverPhone: this.form.get(['receiverPhone']).value,
            notice: this.form.get(['notice']).value,
        };
    }

    compareNotice(first: Notice, second: Notice): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackNoticeById(index: number, item: Notice) {
        return item.id;
    }
}
