import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { NoticeReceiver } from './notice-receiver.model';

@Injectable({ providedIn: 'root'})
export class NoticeReceiverService {
    private resourceUrl = ApiService.API_URL + '/notice-receivers';

    constructor(protected http: HttpClient) { }

    create(noticeReceiver: NoticeReceiver): Observable<HttpResponse<NoticeReceiver>> {
        return this.http.post<NoticeReceiver>(this.resourceUrl, noticeReceiver, { observe: 'response'});
    }

    update(noticeReceiver: NoticeReceiver): Observable<HttpResponse<NoticeReceiver>> {
        return this.http.put(this.resourceUrl, noticeReceiver, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<NoticeReceiver>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<NoticeReceiver[]>> {
        const options = createRequestOption(req);
        return this.http.get<NoticeReceiver[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
