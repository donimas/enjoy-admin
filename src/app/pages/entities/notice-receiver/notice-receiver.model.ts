import { BaseEntity } from 'src/model/base-entity';
import { Notice } from '../notice/notice.model';

export class NoticeReceiver implements BaseEntity {
    constructor(
        public id?: number,
        public toUserId?: number,
        public userLang?: string,
        public flagSmsSent?: boolean,
        public flagEmailSent?: boolean,
        public flagPushSent?: boolean,
        public flagRead?: boolean,
        public readDate?: any,
        public receiverFullName?: string,
        public receiverPhone?: string,
        public notice?: Notice,
    ) {
        this.flagSmsSent = false;
        this.flagEmailSent = false;
        this.flagPushSent = false;
        this.flagRead = false;
    }
}
