import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { NoticeReceiverPage } from './notice-receiver';
import { NoticeReceiverUpdatePage } from './notice-receiver-update';
import { NoticeReceiver, NoticeReceiverService, NoticeReceiverDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class NoticeReceiverResolve implements Resolve<NoticeReceiver> {
  constructor(private service: NoticeReceiverService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<NoticeReceiver> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<NoticeReceiver>) => response.ok),
        map((noticeReceiver: HttpResponse<NoticeReceiver>) => noticeReceiver.body)
      );
    }
    return of(new NoticeReceiver());
  }
}

const routes: Routes = [
    {
      path: '',
      component: NoticeReceiverPage,
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: 'new',
      component: NoticeReceiverUpdatePage,
      resolve: {
        data: NoticeReceiverResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/view',
      component: NoticeReceiverDetailPage,
      resolve: {
        data: NoticeReceiverResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    },
    {
      path: ':id/edit',
      component: NoticeReceiverUpdatePage,
      resolve: {
        data: NoticeReceiverResolve
      },
      data: {
        authorities: ['ROLE_USER']
      },
      canActivate: [UserRouteAccessService]
    }
  ];


@NgModule({
    declarations: [
        NoticeReceiverPage,
        NoticeReceiverUpdatePage,
        NoticeReceiverDetailPage
    ],
    imports: [
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})
export class NoticeReceiverPageModule {
}
