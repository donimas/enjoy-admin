import { BaseEntity } from 'src/model/base-entity';
import { Category } from '../category/category.model';
import { PriceHistory } from '../price-history/price-history.model';

export class Menu implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public imageContentType?: string,
        public image?: any,
        public createDate?: any,
        public flagDeleted?: boolean,
        public content?: string,
        public spicy?: boolean,
        public onStop?: boolean,
        public prepDuration?: number,
        public category?: Category,
        public price?: PriceHistory,
        public combo?: any,
    ) {
        this.flagDeleted = false;
        this.spicy = false;
        this.onStop = false;
    }
}
