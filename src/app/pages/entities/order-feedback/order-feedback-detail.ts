import { Component, OnInit } from '@angular/core';
import { OrderFeedback } from './order-feedback.model';
import { OrderFeedbackService } from './order-feedback.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'page-order-feedback-detail',
    templateUrl: 'order-feedback-detail.html'
})
export class OrderFeedbackDetailPage implements OnInit {
    orderFeedback: OrderFeedback = {};

    constructor(
        private navController: NavController,
        private orderFeedbackService: OrderFeedbackService,
        private activatedRoute: ActivatedRoute,
        private alertController: AlertController
    ) { }

    ngOnInit(): void {
        this.activatedRoute.data.subscribe((response) => {
            this.orderFeedback = response.data;
        });
    }

    open(item: OrderFeedback) {
        this.navController.navigateForward('/tabs/entities/order-feedback/' + item.id + '/edit');
    }

    async deleteModal(item: OrderFeedback) {
        const alert = await this.alertController.create({
            header: 'Confirm the deletion?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.orderFeedbackService.delete(item.id).subscribe(() => {
                            this.navController.navigateForward('/tabs/entities/order-feedback');
                        });
                    }
                }
            ]
        });
        await alert.present();
    }


}
