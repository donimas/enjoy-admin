import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { OrderFeedback } from './order-feedback.model';

@Injectable({ providedIn: 'root'})
export class OrderFeedbackService {
    private resourceUrl = ApiService.API_URL + '/order-feedbacks';

    constructor(protected http: HttpClient) { }

    create(orderFeedback: OrderFeedback): Observable<HttpResponse<OrderFeedback>> {
        return this.http.post<OrderFeedback>(this.resourceUrl, orderFeedback, { observe: 'response'});
    }

    update(orderFeedback: OrderFeedback): Observable<HttpResponse<OrderFeedback>> {
        return this.http.put(this.resourceUrl, orderFeedback, { observe: 'response'});
    }

    find(id: number): Observable<HttpResponse<OrderFeedback>> {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    query(req?: any): Observable<HttpResponse<OrderFeedback[]>> {
        const options = createRequestOption(req);
        return this.http.get<OrderFeedback[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }
}
