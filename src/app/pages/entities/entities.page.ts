import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-entities',
  templateUrl: 'entities.page.html',
  styleUrls: ['entities.page.scss']
})
export class EntitiesPage {
  entities: Array<any> = [
    {name: 'Menu', component: 'MenuPage', route: 'menu'},
    {name: 'Category', component: 'CategoryPage', route: 'category'},
    {name: 'PriceHistory', component: 'PriceHistoryPage', route: 'price-history'},
    {name: 'Order', component: 'OrderPage', route: 'order'},
    {name: 'Location', component: 'LocationPage', route: 'location'},
    {name: 'OrderItem', component: 'OrderItemPage', route: 'order-item'},
    {name: 'OrderStatusHistory', component: 'OrderStatusHistoryPage', route: 'order-status-history'},
    {name: 'OrderFeedback', component: 'OrderFeedbackPage', route: 'order-feedback'},
    {name: 'RatingTag', component: 'RatingTagPage', route: 'rating-tag'},
    {name: 'Promocode', component: 'PromocodePage', route: 'promocode'},
    {name: 'PromocodeSale', component: 'PromocodeSalePage', route: 'promocode-sale'},
    {name: 'Advert', component: 'AdvertPage', route: 'advert'},
    {name: 'Notice', component: 'NoticePage', route: 'notice'},
    {name: 'NoticeReceiver', component: 'NoticeReceiverPage', route: 'notice-receiver'},
    /* jhipster-needle-add-entity-page - JHipster will add entity pages here */
  ];

  constructor(public navController: NavController) {}

  openPage(page) {
    this.navController.navigateForward('/tabs/entities/' + page.route);
  }
}
