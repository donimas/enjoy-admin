import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { JhiDataUtils } from 'ng-jhipster';
import { Advert } from './advert.model';
import { AdvertService } from './advert.service';

@Component({
    selector: 'page-advert',
    templateUrl: 'advert.html'
})
export class AdvertPage {
    adverts: Advert[];

    // todo: add pagination

    constructor(
        private dataUtils: JhiDataUtils,
        private navController: NavController,
        private advertService: AdvertService,
        private toastCtrl: ToastController,
        public plt: Platform
    ) {
        this.adverts = [];
    }

    ionViewWillEnter() {
        this.loadAll();
    }

    async loadAll(refresher?) {
        this.advertService.query().pipe(
            filter((res: HttpResponse<Advert[]>) => res.ok),
            map((res: HttpResponse<Advert[]>) => res.body)
        )
        .subscribe(
            (response: Advert[]) => {
                this.adverts = response;
                if (typeof(refresher) !== 'undefined') {
                    setTimeout(() => {
                        refresher.target.complete();
                    }, 750);
                }
            },
            async (error) => {
                console.error(error);
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Advert) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    new() {
        this.navController.navigateForward('/tabs/entities/advert/new');
    }

    edit(item: IonItemSliding, advert: Advert) {
        this.navController.navigateForward('/tabs/entities/advert/' + advert.id + '/edit');
        item.close();
    }

    async delete(advert) {
        this.advertService.delete(advert.id).subscribe(async () => {
            const toast = await this.toastCtrl.create(
                {message: 'Advert deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    view(advert: Advert) {
        this.navController.navigateForward('/tabs/entities/advert/' + advert.id + '/view');
    }
}
